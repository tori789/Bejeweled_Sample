Visual studio 2019 solution and source files of a bejeweled clone programmed with C++ and the following libraries:

1 - SDL 2.0.7

2 - SDL2_image-2.0.2 using libpng and zlib

3 - SDL2_ttf-2.0.14

I have included them in this repository.



[![Semantic description of image](match3_0.gif "Match 3")**]