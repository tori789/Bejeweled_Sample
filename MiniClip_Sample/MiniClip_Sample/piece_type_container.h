#pragma once

#include <iostream>

namespace bejeweled
{
	/*
		Struct that stores information about a specific piece type.
	*/
	struct PieceTypeContainer
	{
		// Number of pieces present in the world
		int count;

		// Count divided by the total tile count
		float density;

		// Current probability of selecting this piece, range of 0 - 100
		int probability;

		// Integer representing a piece type/ID
		int piece_id;

		PieceTypeContainer() :
			count(-1),
			density(count / (float)(TOTAL_TILE_COUNT)),
			probability(50),
			piece_id(-1) {}

		PieceTypeContainer(int value, int id) :
			count(value),
			density(count / (float)(TOTAL_TILE_COUNT)),
			probability(50),
			piece_id(id) {}

		bool Valid() 
		{
			return piece_id != -1;
		}
	};
}