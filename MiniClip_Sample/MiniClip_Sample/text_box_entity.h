#pragma once

#include "quad_entity.h"
#include "text_texture.h"
#include "texture_manager.h"
#include "math.h"
#include "extensions.h"

namespace bejeweled
{
	/*
		Renders a background texture and then a text over it.
	*/
	class TextBoxEntity
	{
		// The actual text being rendered
		const Texture* text_texture;

		std::string text;
		std::string background_text;

		SDL_Rect textrect;

		// A simple quad with a background texture
		QuadEntity quad;
	public:

		TextBoxEntity(int x, int y, int x_size, int y_size, std::string text, std::string background_text = "score_background") :
			quad(x, y, x_size, y_size), text(text), background_text(background_text), text_texture(nullptr), textrect() {}

		~TextBoxEntity() {}

		void OnStart(SDL_Renderer* g_renderer);
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration);

		// Destroys the existing text and forces the rendering and creation of a new text texture.
		void UpdateText(SDL_Renderer* g_renderer, std::string new_text);
	};
}