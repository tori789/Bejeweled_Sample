#include "texture_manager.h"

namespace bejeweled 
{
	const Texture* TextureManager::Texture(size_t id)
	{
		auto iter = textures.find(id);
		if (iter == textures.end())
		{
			return nullptr;
		}
		else
		{
			return iter->second.get();
		}
	}

	const Texture* TextureManager::CreateTexture(SDL_Renderer* g_renderer, std::string filename)
	{
		const auto iter = Texture(next_unique_texture_id);

		std::unique_ptr<bejeweled::Texture> t = std::make_unique<bejeweled::Texture>(next_unique_texture_id);
		t->LoadFromFile(g_renderer, filename + ".png");
		return textures.emplace(next_unique_texture_id++, std::move(t)).first->second.get();
	}

	const Texture* TextureManager::CreateTexture(SDL_Renderer* g_renderer, std::string filename, size_t id)
	{
		const auto iter = Texture(id);
		if (iter != nullptr)
		{
			return iter;
		}
		else
		{
			std::unique_ptr<bejeweled::Texture> t = std::make_unique<bejeweled::Texture>(id);
			t->LoadFromFile(g_renderer, filename + ".png");
			return textures.emplace(id, std::move(t)).first->second.get();
		}
	}

	const Texture* TextureManager::CreateTextTexture(SDL_Renderer* g_renderer, std::string font, std::string text)
	{
		std::unique_ptr<bejeweled::Texture> t = std::make_unique<TextTexture>(text, next_unique_texture_id);
		t->LoadFromFile(g_renderer, font + ".ttf");
		return textures.emplace(next_unique_texture_id++, std::move(t)).first->second.get();
	}

	const Texture* TextureManager::CreateTextTexture(SDL_Renderer* g_renderer, std::string font, std::string text, size_t id)
	{
		auto iter = Texture(id);
		if (iter != nullptr)
		{
			return iter;
		}
		else
		{
			std::unique_ptr<bejeweled::Texture> t = std::make_unique<TextTexture>(text, id);
			t->LoadFromFile(g_renderer, font + ".ttf");
			return textures.emplace(id, std::move(t)).first->second.get();
		}
	}

	void TextureManager::DestroyTexture(size_t id)
	{
		textures.erase(id);
	}
}