#include "text_entity.h"

namespace bejeweled
{
	void TextEntity::OnStart(SDL_Renderer* g_renderer)
	{
		std::string font = "pixel_font";
		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
		quad.texture = TextureManager::GetInstance().CreateTextTexture(g_renderer, font, text);
		UpdateText(g_renderer, text);
	}

	void TextEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		// Render the text
		Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, TILE_PIXEL_SIZE);
	}

	void TextEntity::UpdateText(SDL_Renderer* g_renderer, std::string new_text)
	{
		if (new_text.compare(text) == 0)
		{
			return;
		}
		this->text = new_text;

		std::string font = "pixel_font";
		int id = quad.texture->ID();
		TextureManager::GetInstance().DestroyTexture(id);
		quad.texture = TextureManager::GetInstance().CreateTextTexture(g_renderer, font, text, id);

		float aspect_ratio = (float)quad.texture->Width() / quad.texture->Height();

		int text_width = 0;
		int text_height = 0;

		text_height = quad.y_size;
		text_width = (int)(text_height * aspect_ratio);

		if (text_width > quad.x_size)
		{
			text_width = quad.x_size;
			text_height = (int)(text_width / aspect_ratio);
		}

		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE + (int)(quad.x_size * 0.5f) - (int)(text_width * 0.5f), quad.y_pos_index * TILE_PIXEL_SIZE - (int)(quad.y_size * 0.5f) + (int)(text_height * 0.5f), text_width, text_height };
	}
}