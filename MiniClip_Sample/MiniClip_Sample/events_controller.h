#pragma once

#include <memory>  
#include "tile_entity.h"
#include "world_controller.h"
#include "ui_controller.h"

namespace bejeweled
{
	/*
		Events controller class that manages the player input like:
		1 - piece selection.
		2 - drag and drop of pieces.
	*/
	class EventsController
	{
		int mouse_x;
		int mouse_y;

		bool dragging;
		bool selected;
		bool clicked_down;

		TileEntity* selected_tile;

		bool game_over;

	public:
		EventsController() : mouse_x(0), mouse_y(0), dragging(false), selected(false), clicked_down(false), game_over(false), selected_tile(nullptr) {}
		~EventsController() {}

		// Main Event function
		void OnEvent(SDL_Event e, WorldController& world, UiController& ui);

		void GameOver();
		void Restart();
	};
}