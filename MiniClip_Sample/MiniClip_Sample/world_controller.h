#pragma once

#include <vector>
#include <tuple>
#include <memory>
#include "extensions.h"
#include "math.h"
#include "tile_entity.h"
#include "constants.h"
#include "piece_type_container.h"
#include "map_entity.h"
#include "tile_entity.h"
#include "effects_controller.h"
#include "piece_switch.h"

namespace bejeweled
{
	/*
		Main controller class responsible for:
		1 - map initialization and rendering.
		2 - explosion creation and rendering.
		3 - matching, consolidation and filling of empty tiles.
		4 - setting up movement of pieces.
	*/
	class WorldController : public Renderable
	{
		// Map class containing tiles which contain pieces.
		MapEntity map;

		// Explosion controller.
		EffectsController effects;

		// Collection of pieces to check for matching in the next OnRender cycle.
		std::vector<PieceSwitch> potential_matching_tiles;
		std::vector<PieceTypeContainer> piece_types;

		// Number of total destroyed pieces so far, used to calculate score.
		int destroyed_pieces;

		// Function that takes two tiles and returns true if a match occurs.
		bool Match(TileEntity& current_tile, TileEntity& previous_tile, SDL_Renderer* g_renderer);

		// Creates a score explosion.
		void ExplodeScore(SDL_Renderer* g_renderer, const TileEntity& tile, int number_of_pieces);

		// Destroys a piece and instantiates an explosion in that position.
		void DestroyAndExplodePiece(SDL_Renderer* g_renderer, TileEntity& tile);

		// Calculate a number of total possible moves from the current map.
		int CalculateMoves();

		// Iterates the entire map and fills in empty tiles.
		void FillMissingTiles(SDL_Renderer* g_renderer);

		// Checks the y column of the current tile and returns a list of matched tiles
		std::vector<std::reference_wrapper<const PieceEntity>> MatchVertical(TileEntity& current_tile);

		// Checks the x line of the current tile and returns a list of matched tiles
		std::vector<std::reference_wrapper<const PieceEntity>>  MatchHorizontal(TileEntity& current_tile);

		// Checks if there are two other tiles with the same type/id up, down, left or right of a tile.
		bool CheckNeighbors(int piece_id, TileEntity& tile, int amount);

		// Checks if there are two other tiles with the same type/id with X or Y increments from the current tile within a maximum distance.
		bool CheckNeighbors(int piece_id, TileEntity& tile, int x_increment, int y_increment, int distance);

		// Iterates all piece_types and randomly determines a new piece type, the probability is weighted by the number of pieces each type has.
		int CalculateNewPieceType(TileEntity& tile);

		// Iterates the map and sets pieces in motion if there are empty tiles beneath them.
		void ConsolidadeTiles();

	public:
		WorldController(int x_tiles, int y_tiles);
		~WorldController();

		// Random initialization of the map with pieces.
		void RandomFill(SDL_Renderer* g_renderer);

		void OnStart(SDL_Renderer* g_renderer) override;
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) override;

		// Resets the world, deletes all pieces.
		void GameOver(SDL_Renderer* g_renderer);

		// Instantly switches the pieces
		void Switch(TileEntity& current_tile, TileEntity& next_tile);

		// Sets the pieces in motion and switches them on the tile
		void AnimatedSwitch(TileEntity& current_tile, TileEntity& next_tile);

		// Adds a tuple of tiles to check for matching
		void AddPotentialMatch(TileEntity& current_tile, TileEntity& next_tile);

		TileEntity& TileFromMouseCoordinates(int x, int y);
		bool HasHorizontalMovingPieces(TileEntity& tile);
		bool HasVerticalMovingPieces(TileEntity& tile);

		// Do we have matches we haven't checked?
		bool HasPotentialMatches() const;

		int DestroyedPieces() const;
	};
}