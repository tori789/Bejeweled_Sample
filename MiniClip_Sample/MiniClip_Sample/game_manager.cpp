#include "game_manager.h"

namespace bejeweled 
{
	void GameManager::OnInit()
	{
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			std::cout << "SDL could not initialize! SDL Error: " + std::string(SDL_GetError()) << std::endl;
			return;
		}

		// Create window 
		g_window = SDL_CreateWindow("Bejeweled Sample Andre Bico", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH + MARGIN_X, SCREEN_HEIGHT + MARGIN_Y, SDL_WINDOW_SHOWN);
		if (g_window == NULL)
		{
			std::cout << "Window could not be created! SDL_Error: " + std::string(SDL_GetError()) << std::endl;
			return;
		}

		// Create 2d rendering context for the created window
		g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (g_renderer == NULL)
		{
			std::cout << "Renderer could not be created! SDL Error: " + std::string(SDL_GetError()) << std::endl;
			return;
		}

		SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);

		// Initialize PNG loading
		int imgFlags = IMG_INIT_PNG;
		if (!(IMG_Init(imgFlags) & imgFlags))
		{
			std::cout << "SDL_image could not initialize! SDL_image Error: " + std::string(IMG_GetError()) << std::endl;
			return;
		}

		// Initialize TTF library
		if (TTF_Init() < 0) {
			std::cout << "TTF library could not initialize! SDL_image Error: " + std::string(TTF_GetError()) << std::endl;
			return;
		}
	}

	void GameManager::OnStart()
	{
		ui.OnStart(g_renderer);
		world.OnStart(g_renderer);
	}

	void GameManager::OnEvent()
	{
		//Event handler
		SDL_Event e;

		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				running = false;
				continue;
			}
			// Handle keypress with current mouse position
			if (gameover && e.type == SDL_EventType::SDL_MOUSEBUTTONDOWN)
			{
				Restart();
				continue;
			}
			events.OnEvent(e, world, ui);
		}
	}

	void GameManager::OnRender()
	{
		int start = 0;
		int last_frame_duration = 0;
		// 60 fps

		while (running)
		{
			start = (int)SDL_GetPerformanceCounter();

			// End Condition;
			if (!gameover)
			{
				if (timer < 0 || score >= VICTORY_SCORE)
				{
					events.GameOver();
					ui.GameOver(g_renderer, score);
					world.GameOver(g_renderer);
					gameover = true;
				}
				else
				{
					timer -= last_frame_duration;
				}
			}

			// Handle all events
			OnEvent();

			// Clear render target
			SDL_SetRenderDrawColor(g_renderer, 30, 30, 30, 30);
			SDL_RenderClear(g_renderer);

			// Keep track of score
			score = world.DestroyedPieces() * SCORE_PER_PIECE;

			// Render the world
			world.OnRender(g_renderer, last_frame_duration);

			if (!gameover)
			{
				ui.UpdateScore(g_renderer, score);
				ui.UpdateTimer(g_renderer, timer / 1000);
			}

			// Render the UI
			ui.OnRender(g_renderer, last_frame_duration);

			//Update screen
			SDL_RenderPresent(g_renderer);

			int now = (int)SDL_GetPerformanceCounter();
			float frequency = (float)SDL_GetPerformanceFrequency();
			last_frame_duration = (int)(((now - start) / frequency) * 1000);
		}
	}

	void GameManager::OnDelete()
	{
		SDL_DestroyRenderer(g_renderer);
		SDL_DestroyWindow(g_window);
		g_window = nullptr;
		g_renderer = nullptr;

		// Quite text rendering
		TTF_Quit();

		// Quit image SDL
		IMG_Quit();

		//Quit SDL subsystems
		SDL_Quit();
	}

	void GameManager::Restart()
	{
		events.Restart();
		world.OnStart(g_renderer);
		ui.Restart();
		gameover = false;
		score = 0;
		timer = TIME * 1000;
	}

	GameManager::~GameManager()
	{

	}
}