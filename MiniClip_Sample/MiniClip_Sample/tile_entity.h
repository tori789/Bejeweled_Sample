#pragma once

#include "piece_entity.h"
#include "extensions.h"

namespace bejeweled
{
	/*
		Class representing a tile/position on the world map.
		Contains a piece.
	*/
	class TileEntity : public Renderable
	{
		// The piece inside of the tile
		PieceEntity piece;

		// Square representing the tile itself
		QuadEntity quad;
	public:
		TileEntity(int x, int y, int size) : quad(x, y, size, size) {}
		// Default invalid tile
		TileEntity() : quad(-1, -1,-1, -1){}
		~TileEntity() = default;

		bool operator!=(TileEntity const& other)
		{
			return quad.X() != other.quad.X() || quad.Y() != other.quad.Y();
		}

		bool operator==(TileEntity const& other)
		{
			return quad.X() == other.quad.X() && quad.Y() == other.quad.Y();
		}
	public:
		void OnStart(SDL_Renderer* g_renderer) override;
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) override;

		// Fill a tile with an entirely new piece
		void FillWithPiece(PieceEntity& new_piece);

		void DestroyPiece();

		bool HasPiece() const;

		// Returns true if another tile is Up, down, left or right of the current tile.
		bool IsAdjacent(TileEntity& other_tile) const;

		const PieceEntity& Piece() const;
		PieceEntity& Piece();

		int X() const;
		int Y() const;
	};
}