#pragma once

#include "math.h"
#include "quad_entity.h"
#include "texture_manager.h"
#include "extensions.h"

namespace bejeweled
{
	/*
		Class thats responsible for rendering and animating a piece.
	*/
	class PieceEntity : public Renderable
	{
		// Piece id.
		int piece_id;

		// Current pixel position of the object.
		int x_current_pixel;
		int y_current_pixel;

		// Active goal in index coordinates.
		int x_goal_index;
		int y_goal_index;

		// Previous goal in index coordinates.
		int x_previous_goal_index;
		int y_previous_goal_index;

		// True when a piece is moving towards a position.
		bool updatePosition;

		// Time in miliseconds it takes a piece to move from one goal to another.
		int animation_time;

		// Time in miliseconds the piece has started moving.
		int time_moving;

		// If disabled we do not render the current piece.
		bool enabled;

		QuadEntity quad;

	public:

		PieceEntity(int x, int y, int size, int piece_id, int animation_time = 200, bool enabled = true) :
			quad(x, y, size, size),
			piece_id(piece_id),
			x_current_pixel(x * TILE_PIXEL_SIZE),
			y_current_pixel(y * TILE_PIXEL_SIZE),
			x_goal_index(x),
			y_goal_index(y),
			updatePosition(false),
			animation_time(animation_time), time_moving(0),
			x_previous_goal_index(x),
			y_previous_goal_index(y),
			enabled(enabled) {}
		PieceEntity() : PieceEntity(0, 0, 0, -1) {}

		~PieceEntity() {}

		void OnStart(SDL_Renderer* g_renderer) override;
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) override;

		void Move(int x_new_pos, int y_new_pos);
		void SetPosition(int x_new_pos, int y_new_pos);

		int PieceID() const;

		bool SameID(int piece_type) const;
		bool IsMoving() const;

		void Enable();
		void Disable();

		int X() const;
		int Y() const;

		int PreviousX() const;
		int PreviousY() const;

		bool Valid() const;
	};
}