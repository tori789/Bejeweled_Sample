#pragma once

#include "quad_entity.h"
#include "text_texture.h"
#include "texture_manager.h"
#include "math.h"
#include "extensions.h"

namespace bejeweled
{
	/*
		Responsible for rendering a text.
	*/
	class TextEntity
	{
		std::string text;

		QuadEntity quad;

	public:

		TextEntity(int x, int y, int x_size, int y_size, std::string text) :
			quad(x, y, x_size, y_size), text(text) {}

		~TextEntity() {}

		void OnStart(SDL_Renderer* g_renderer);
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration);

		// Destroys the existing text and forces the rendering and creation of a new text texture.
		void UpdateText(SDL_Renderer* g_renderer, std::string new_text);
	};
}