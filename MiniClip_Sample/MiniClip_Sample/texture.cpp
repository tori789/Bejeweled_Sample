#include "texture.h"

namespace bejeweled 
{
	Texture::~Texture()
	{
		SDL_DestroyTexture(sdl_texture);
	}

	void Texture::LoadFromFile(SDL_Renderer* g_renderer, std::string file_name)
	{
		this->file = file_name;
		SDL_Surface* sdl_surface = IMG_Load(file_name.c_str());
		if (sdl_surface == nullptr)
		{
			std::cout << ("Unable to load image " + file_name + " SDL_image Error: " + IMG_GetError()).c_str()  << std::endl;
			return;
		}

		//Color key image
		SDL_SetColorKey(sdl_surface, SDL_TRUE, SDL_MapRGB(sdl_surface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		sdl_texture = SDL_CreateTextureFromSurface(g_renderer, sdl_surface);

		if (sdl_texture == nullptr)
		{
			std::cout << ("Unable to create texture from " + file_name + " SDL Error: " + SDL_GetError()).c_str() << std::endl;
			return;
		}

		SDL_QueryTexture(sdl_texture, NULL, NULL, &width, &height);

		SDL_FreeSurface(sdl_surface);
	}

	bool Texture::Valid() const 
	{
		return sdl_texture != nullptr;
	}

	std::string Texture::TextureFilename() const
	{
		return file;
	}

	SDL_Texture* Texture::SDLTexture() const
	{
		return sdl_texture;
	}

	int Texture::Width() const
	{
		return width;
	}

	int Texture::Height() const
	{
		return height;
	}

	size_t Texture::ID() const
	{
		return id;
	}
}