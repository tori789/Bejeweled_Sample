#pragma once

#include <memory>  
#include <unordered_map>
#include "texture.h"
#include "text_texture.h"
#include "constants.h"

namespace bejeweled 
{
	/*
		Stores all existing textures, manages creation of textures
	*/
	class TextureManager
	{
		// Texture handle to texture instance
		std::unordered_map<size_t, std::unique_ptr<Texture>> textures;
			
		// We have a certain amount of preallocated ids for textures the rest we just increment
		size_t next_unique_texture_id = UNIQUE_TEXTURE_ID;

		TextureManager() : textures{} {}
	public:
		~TextureManager() {}

		static TextureManager& GetInstance()
		{
			static TextureManager texture_manager;
			return texture_manager;
		}

		// Creates a texture with a unique ID, internally managed
		const Texture* CreateTexture(SDL_Renderer* g_renderer, std::string filename);

		// Creates a texture with a specific ID
		const Texture* CreateTexture(SDL_Renderer* g_renderer, std::string filename, size_t id);

		// Creates a text texture with a unique ID, internally managed
		const Texture* CreateTextTexture(SDL_Renderer* g_renderer, std::string font, std::string text);

		// Creates a texture with a specific ID
		const Texture* CreateTextTexture(SDL_Renderer* g_renderer, std::string font, std::string text, size_t id);
		const Texture* Texture(size_t id);
		void DestroyTexture(size_t id);
	};
}