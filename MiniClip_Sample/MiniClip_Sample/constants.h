#pragma once

namespace bejeweled 
{
	static constexpr int SCREEN_WIDTH = 640;
	static constexpr int SCREEN_HEIGHT = 512;

	static constexpr int TILE_PIXEL_SIZE = 64;
	static constexpr int HALF_TILE_PIXEL_SIZE = 32;

	// We create a window of size WIDTH and HEIGHT with the following extra margins.
	static constexpr int MARGIN_X = TILE_PIXEL_SIZE * 3;
	static constexpr int MARGIN_Y = TILE_PIXEL_SIZE * 2;

	static constexpr int MAP_TILE_SIZE_X = 8;
	static constexpr int MAP_TILE_SIZE_Y = 8;

	static constexpr int TOTAL_TILE_COUNT = MAP_TILE_SIZE_X * MAP_TILE_SIZE_Y;

	// When a piece is destroyed this is the amount of score it gives.
	static constexpr int SCORE_PER_PIECE = 100;

	static constexpr int TIME = 200;
	static constexpr int VICTORY_SCORE = 5000;

	// Offset applied to every 2d entity, works like a camera moving the world by OFFSET amount.
	static constexpr int OFFSET_X = TILE_PIXEL_SIZE;
	static constexpr int OFFSET_Y = TILE_PIXEL_SIZE;

	static constexpr int UNIQUE_TEXTURE_ID = 2;
	static constexpr int SPRITE_SHEET_TEXTURE_ID = 1;
}