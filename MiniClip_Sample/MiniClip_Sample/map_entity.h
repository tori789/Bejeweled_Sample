#pragma once

#include <vector>
#include <memory>
#include "tile_entity.h"

namespace bejeweled
{
	/*
		Class containing a collection of tiles and their respective pieces.
	*/
	class MapEntity : Renderable
	{
		std::vector<TileEntity> tiles;

	public:
		MapEntity();
		~MapEntity();

		// Initializes the tiles and then the pieces contained in them.
		void OnStart(SDL_Renderer* g_renderer);

		// Renders tiles and then the pieces contained in them.
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration);

		// Helper functions to obtain tiles.
		TileEntity& TileFromIndex(int index);
		TileEntity& TileFromXY(int x, int y);

		TileEntity& Right(const TileEntity& current_tile);
		TileEntity& Left(const TileEntity& current_tile);
		TileEntity& Up(const TileEntity& current_tile);
		TileEntity& Down(const TileEntity& current_tile);
	};
}