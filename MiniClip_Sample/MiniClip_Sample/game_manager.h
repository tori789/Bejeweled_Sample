#pragma once

struct SDL_Window;
struct SDL_Renderer;

#include <iostream> 
#include "constants.h"
#include "world_controller.h"
#include "events_controller.h"
#include "ui_controller.h"
#include "effects_controller.h"
#include "constants.h"

namespace bejeweled 
{
	/*
		Principal manager class responsible for creating and initializing a SDL window and renderer.
		It also manages the gamestate with three controller classes, world, events and ui.
	*/
	class GameManager
	{
		// Score achieved so far.
		int score;

		// Time the player has to finish the game.
		int timer;

		// SDL window created in OnStart.
		SDL_Window* g_window;
		SDL_Renderer* g_renderer;

		// Handles player input on the world and ui.
		EventsController events;

		// Responsible for initializing and rendering the ingame Map, Tiles and pieces.
		WorldController world;

		// Contains the different ui elements controlls their initialization, update and rendering.
		UiController ui;

		// Render loop runs while running is true.
		bool running;

		// Gets updated when the game is over
		bool gameover;

	public:
		GameManager() : score(0), timer(TIME * 1000), running(true), gameover(false), g_window(nullptr), g_renderer(nullptr), world(MAP_TILE_SIZE_X, MAP_TILE_SIZE_Y) {}
		~GameManager();

		// Function responsible for:
		// 1 - Window initialization,
		// 2 - 2D SDL rendering context, 
		// 3 - TTF SDL font library
		// 4 - Creating a world controller
		// 5 - Creating a ui controller
		// 6 - Creating a events controller
		void OnInit();

		// Main Event check loop
		void OnEvent();

		// Responsible for creating the tile based map
		void OnStart();

		// Main Rendering loop
		void OnRender();

		// Responsible for dealocating SDL
		void OnDelete();

		// Resets score, timer and the world, ui and events controllers
		void Restart();
	};
}