#pragma once

#include <memory>
#include <SDL.h>
#include "renderable.h"
#include "tile_entity.h"
#include "text_box_entity.h"
#include "extensions.h"
#include "explosion_entity.h"
#include "score_explosion_entity.h"

namespace bejeweled
{
	/*
		Responsible for creating, initializing and rendering explosions - (pieces that scale to zero).
	*/
	class EffectsController : public Renderable
	{
		std::vector<std::unique_ptr<ExplosionEntity>> effects;

	public:
		EffectsController() : effects{} {}
		~EffectsController() {}

		void OnStart(SDL_Renderer* g_renderer);
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration);

		void CreateExplosion(SDL_Renderer* g_renderer, const TileEntity& tile);
		void CreateScoreExplosion(SDL_Renderer* g_renderer, const TileEntity& tile, int num_pieces);
	};
}