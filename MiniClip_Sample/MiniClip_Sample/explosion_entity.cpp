#include "explosion_entity.h"

namespace bejeweled
{
	void ExplosionEntity::OnStart(SDL_Renderer* g_renderer)
	{
		quad.texture = TextureManager::GetInstance().CreateTexture(g_renderer, "sprite_sheet", SPRITE_SHEET_TEXTURE_ID);
		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
		quad.originrect = { TILE_PIXEL_SIZE * piece_id, 0, quad.x_size, quad.y_size };
	}

	void ExplosionEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		float percentageComplete = (float)time_moving / animation_time;

		if (percentageComplete >= 1.0f)
		{
			finished = true;
			time_moving = 0;
			return;
		}

		// We lerp our scale towards 0
		float new_scale = math::Lerp(1.0f, 0.0f, math::CubicEaseInOut(percentageComplete));

		int new_x_size = (int)(quad.x_size * new_scale);
		int new_y_size = (int)(quad.y_size * new_scale);

		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE + HALF_TILE_PIXEL_SIZE - (int)(new_x_size * 0.5f),
						quad.y_pos_index * TILE_PIXEL_SIZE + HALF_TILE_PIXEL_SIZE + (int)(new_y_size * 0.5f),
						new_x_size, new_y_size };

		time_moving += last_frame_duration;
		Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, quad.originrect, 0);
	}

	bool ExplosionEntity::Finished() const
	{
		return finished;
	}
}