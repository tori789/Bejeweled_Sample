#include "map_entity.h"

namespace bejeweled
{
	MapEntity::MapEntity() : tiles{}
	{
		// First we create a simple tile map
		for (int y = 0; y < MAP_TILE_SIZE_Y; y++)
		{
			for (int x = 0; x < MAP_TILE_SIZE_X; x++)
			{
				tiles.emplace_back(x, y, TILE_PIXEL_SIZE);
			}
		}
	}

	MapEntity::~MapEntity()
	{

	}

	void MapEntity::OnStart(SDL_Renderer* g_renderer)
	{
		// Start all tiles
		for (auto& tile : tiles)
		{
			tile.OnStart(g_renderer);
		}

		// Start all pieces
		for (auto& tile : tiles)
		{
			tile.Piece().OnStart(g_renderer);
		}
	}

	void MapEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		// Render tiles
		for (auto& tile : tiles)
		{
			tile.OnRender(g_renderer, last_frame_duration);
		}

		// Render pieces
		for (auto& tile : tiles)
		{
			if (tile.HasPiece())
			{
				tile.Piece().OnRender(g_renderer, last_frame_duration);
			}
		}
	}

	TileEntity& MapEntity::TileFromIndex(int index)
	{
		assert(index >= 0 && index < TOTAL_TILE_COUNT);
		return tiles.at(index);
	}

	// Returns a tile from index x and index y values
	TileEntity& MapEntity::TileFromXY(int x, int y)
	{
		assert(x >= 0 && x <= MAP_TILE_SIZE_X - 1 && y >= 0 && y <= MAP_TILE_SIZE_Y);
		return tiles.at(x + y * MAP_TILE_SIZE_X);
	}

	TileEntity& MapEntity::Right(const TileEntity& current_tile)
	{
		int index = current_tile.X() + 1 + current_tile.Y() * MAP_TILE_SIZE_X;
		assert(index >= 0 && index < TOTAL_TILE_COUNT);
		return tiles.at(index);
	}

	TileEntity& MapEntity::Left(const TileEntity& current_tile)
	{
		int index = current_tile.X() - 1 + current_tile.Y() * MAP_TILE_SIZE_X;
		assert(index >= 0 && index < TOTAL_TILE_COUNT);
		return tiles.at(index);
	}

	TileEntity& MapEntity::Up(const TileEntity& current_tile)
	{
		int index = current_tile.X() + (current_tile.Y() + 1) * MAP_TILE_SIZE_X;
		assert(index >= 0 && index < TOTAL_TILE_COUNT);
		return tiles.at(index);
	}

	TileEntity& MapEntity::Down(const TileEntity& current_tile)
	{
		int index = current_tile.X() + (current_tile.Y() - 1) * MAP_TILE_SIZE_X;
		assert(index >= 0 && index < TOTAL_TILE_COUNT);
		return tiles.at(index);
	}
}