#pragma once

#include "quad_entity.h"
#include "text_texture.h"
#include "texture_manager.h"
#include "math.h"
#include "extensions.h"

namespace bejeweled
{
	/*
		Renders the scaling texture acording to the amount of score we have left to achieve.
	*/
	class BarEntity : public Renderable
	{
		const Texture* back_texture;
		SDL_Rect backrect;
		QuadEntity quad;
	public:

		BarEntity(int x, int y, int x_size, int y_size) :
			quad(x, y, x_size, y_size), backrect(), back_texture(nullptr) {}

		~BarEntity() {}

		void OnStart(SDL_Renderer* g_renderer) override;
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) override;

		// Scales the current texture.
		void Scale(float scale);
	};
}