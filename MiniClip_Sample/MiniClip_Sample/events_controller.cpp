#include "events_controller.h"

namespace bejeweled
{
	void EventsController::OnEvent(SDL_Event e, WorldController& world, UiController& ui)
	{
		if (game_over)
		{
			return;
		}

		if (e.type == SDL_EventType::SDL_MOUSEBUTTONDOWN)
		{
			GetMouseCoordinates(&mouse_x, &mouse_y);

			// Update selected tile
			TileEntity& potential_tile = world.TileFromMouseCoordinates(mouse_x, mouse_y);

			// Do we have a selected tile
			if (!(selected && selected_tile != nullptr))
			{
				if (!world.HasHorizontalMovingPieces(potential_tile) && !world.HasPotentialMatches())
				{
					selected = true;
					selected_tile = &potential_tile;
				}
			}
			else
			{
				if (!potential_tile.IsAdjacent(*selected_tile))
				{
					if (!world.HasPotentialMatches())
					{
						selected = true;
						selected_tile = &potential_tile;
					}
				}
			}
			// We cant select yet we can only do it on the mouse up
			clicked_down = true;
			return;
		}
		if (e.type == SDL_EventType::SDL_MOUSEBUTTONUP)
		{
			if (dragging)
			{
				dragging = false;
			}
			else if (selected && selected_tile != nullptr)
			{
				TileEntity& clickedTile = world.TileFromMouseCoordinates(mouse_x, mouse_y);
				ui.Select(*selected_tile);
				// If so then we check if we are clicking on a adjacent tile
				if (clickedTile.IsAdjacent(*selected_tile) && !world.HasPotentialMatches())
				{
					// We try to switch the tiles and block their acess
					world.AnimatedSwitch(clickedTile, *selected_tile);
					world.AddPotentialMatch(clickedTile, *selected_tile);

					// we just deselect
					selected = false;
					selected_tile = nullptr;
					ui.Deselect();
				}
			}

			clicked_down = false;
			return;
		}
		if (e.type == SDL_EventType::SDL_MOUSEMOTION && clicked_down)
		{
			ui.Deselect();

			int new_mouse_x;
			int new_mouse_y;

			GetMouseCoordinates(&new_mouse_x, &new_mouse_y);

			if (selected && selected_tile != nullptr)
			{
				int offset_x = new_mouse_x - mouse_x;
				int offset_y = new_mouse_y - mouse_y;

				if (std::abs(offset_x) > std::abs(offset_y))
				{
					offset_y = 0;
				}
				else
				{
					offset_x = 0;
				}

				TileEntity& clickedTile = world.TileFromMouseCoordinates(mouse_x + offset_x, mouse_y + offset_y);

				// If so then we check if we are clicking on a adjacent tile
				if (clickedTile.IsAdjacent(*selected_tile) && !world.HasPotentialMatches())
				{
					// We try to switch the tiles and block their acess
					world.AnimatedSwitch(clickedTile, *selected_tile);
					world.AddPotentialMatch(clickedTile, *selected_tile);

					// we just deselect
					selected = false;
					selected_tile = nullptr;
					ui.Deselect();
				}
			}

			dragging = true;
			return;
		}
	}

	void EventsController::GameOver()
	{
		game_over = true;
	}

	void EventsController::Restart()
	{
		game_over = false;
	}
}