#pragma once
#include <vector>
#include <sstream>
#include <SDL.h>
#include "constants.h"
#include "math.h"

namespace bejeweled 
{
	template <typename T> T RemoveAt(std::vector<T>&v, typename std::vector<T>::size_type n)
	{
		T ans = std::move_if_noexcept(v[n]);
		v[n] = std::move_if_noexcept(v.back());
		v.pop_back();
		return ans;
	}

	template <typename T> std::string ToString(T Number)
	{
		std::ostringstream ss;
		ss << Number;
		return ss.str();
	}

	//TODO rethink inline on these two methods
	inline void Render(SDL_Renderer* g_renderer, SDL_Texture* texture, SDL_Rect fillrect, SDL_Rect srcrect, int offset)
	{
		SDL_Rect flipedRect = { OFFSET_X + fillrect.x, OFFSET_Y + SCREEN_HEIGHT - offset - fillrect.y, fillrect.w, fillrect.h };
		SDL_RenderCopy(g_renderer, texture, &srcrect, &flipedRect);
	}

	inline void Render(SDL_Renderer* g_renderer, SDL_Texture* texture, SDL_Rect fillrect, int offset)
	{
		SDL_Rect flipedRect = { OFFSET_X + fillrect.x, OFFSET_Y + SCREEN_HEIGHT - offset - fillrect.y, fillrect.w, fillrect.h };
		SDL_RenderCopy(g_renderer, texture, NULL, &flipedRect);
	}

	inline void GetMouseCoordinates(int* x, int* y)
	{
		int mouse_x, mouse_y;
		SDL_GetMouseState(&mouse_x, &mouse_y);
		mouse_x -= OFFSET_X;
		mouse_y -= OFFSET_Y;
		mouse_y = SCREEN_HEIGHT - mouse_y;
		mouse_x = math::Clamp(mouse_x, 0, TILE_PIXEL_SIZE * MAP_TILE_SIZE_X);
		mouse_y = math::Clamp(mouse_y, 0, TILE_PIXEL_SIZE * MAP_TILE_SIZE_Y);

		*x = mouse_x;
		*y = mouse_y;
	}
}