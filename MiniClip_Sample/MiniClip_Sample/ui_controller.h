#pragma once

#include <memory>
#include <SDL.h>
#include "renderable.h"
#include "tile_entity.h"
#include "text_box_entity.h"
#include "extensions.h"
#include "text_entity.h"
#include "bar_entity.h"

namespace bejeweled
{
	/*
		Contains all the different ui elements present in the game.
	*/
	class UiController : public Renderable
	{
		// Selector piece that is rendered when the user clicks a piece with the mouse.
		PieceEntity selector;

		// Score name.
		TextBoxEntity score_text;

		// Score text updated constantly.
		TextEntity score;

		// Timer name.
		TextBoxEntity timer_text;

		// Timer value updated constantly.
		TextBoxEntity timer;

		// Text to display when the game over condition has been achieved in the game manager.
		TextEntity game_over_text;

		// Restart text.
		TextEntity click_to_restart;

		// Score bar that scales up as we reach the victory score goal.
		BarEntity bar;

		bool game_over;

	public:
		UiController();
		~UiController() {}

		void OnStart(SDL_Renderer* g_renderer) override;
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) override;

		void Select(TileEntity& tile);
		void Deselect();

		void UpdateScore(SDL_Renderer* g_renderer, int score_value);
		void UpdateTimer(SDL_Renderer* g_renderer, int timer_value);

		void GameOver(SDL_Renderer* g_renderer, int score_value);
		void Restart();
	};
}