#include "text_box_entity.h"
#include <functional>

namespace bejeweled
{
	void TextBoxEntity::OnStart(SDL_Renderer* g_renderer)
	{
		std::string font = "pixel_font";
		std::hash<std::string> hasher;

		size_t hash = hasher(background_text);
		quad.texture = TextureManager::GetInstance().CreateTexture(g_renderer, background_text, hash);
		text_texture = TextureManager::GetInstance().CreateTextTexture(g_renderer, font, "0");
		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
		UpdateText(g_renderer, text);
	}

	void TextBoxEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		// Render the background texture first
		Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, TILE_PIXEL_SIZE);

		// Render the text after
		Render(g_renderer, text_texture->SDLTexture(), textrect, TILE_PIXEL_SIZE);
	}

	void TextBoxEntity::UpdateText(SDL_Renderer* g_renderer, std::string new_text)
	{
		if (new_text.compare(text) == 0)
		{
			return;
			
		}
		std::string font = "pixel_font";

		this->text = new_text;

		int id = text_texture->ID();
		TextureManager::GetInstance().DestroyTexture(id);
		text_texture = TextureManager::GetInstance().CreateTextTexture(g_renderer, font, text, id);

		int pixel_outline = 10;
		float aspect_ratio = (float)text_texture->Width() / text_texture->Height();

		int text_width = 0;
		int text_height = 0;

		text_height = quad.y_size - pixel_outline;
		text_width = (int)(text_height * aspect_ratio);

		if (text_width > quad.x_size)
		{
			text_width = quad.x_size - pixel_outline;
			text_height = (int)(text_width / aspect_ratio);
		}

		textrect = { quad.x_pos_index * TILE_PIXEL_SIZE + (int)(quad.x_size * 0.5f) - (int)(text_width * 0.5f), quad.y_pos_index * TILE_PIXEL_SIZE - (int)(quad.y_size * 0.5f) + (int)(text_height * 0.5f), text_width, text_height };
	}
}