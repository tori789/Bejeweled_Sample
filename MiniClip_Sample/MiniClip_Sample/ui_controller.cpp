#include "ui_controller.h"

namespace bejeweled
{
	UiController::UiController() : game_over(false)
		, selector(0, 0, TILE_PIXEL_SIZE, 6, 50, false)		
		// Score
		, score_text(MAP_TILE_SIZE_X + 1, MAP_TILE_SIZE_Y - 1, TILE_PIXEL_SIZE * 2, TILE_PIXEL_SIZE, "Score")
		, score(MAP_TILE_SIZE_X + 1, MAP_TILE_SIZE_Y - 2, TILE_PIXEL_SIZE * 2, TILE_PIXEL_SIZE * 5, "000")
		// Timer
		, timer_text(MAP_TILE_SIZE_X + 1, MAP_TILE_SIZE_Y - 7, TILE_PIXEL_SIZE * 2, TILE_PIXEL_SIZE, "Timer")
		, timer(MAP_TILE_SIZE_X + 1, MAP_TILE_SIZE_Y - 8, TILE_PIXEL_SIZE * 2, TILE_PIXEL_SIZE, ToString(TIME))
		// End screen
		, game_over_text(1, MAP_TILE_SIZE_Y - 2, TILE_PIXEL_SIZE * 6, TILE_PIXEL_SIZE * 4, "Game Over")
		, click_to_restart(1, MAP_TILE_SIZE_Y - 4, TILE_PIXEL_SIZE * 6, TILE_PIXEL_SIZE * 4, "Click To Restart")
		// Bar to fill
		, bar(MAP_TILE_SIZE_X + 1, MAP_TILE_SIZE_Y - 2, TILE_PIXEL_SIZE * 2, TILE_PIXEL_SIZE * 5)
	{
	}

	void UiController::OnStart(SDL_Renderer* g_renderer)
	{
		selector.OnStart(g_renderer);
		score.OnStart(g_renderer);
		score_text.OnStart(g_renderer);
		timer_text.OnStart(g_renderer);
		timer.OnStart(g_renderer);
		game_over_text.OnStart(g_renderer);
		click_to_restart.OnStart(g_renderer);
		bar.OnStart(g_renderer);
	}

	void UiController::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		selector.OnRender(g_renderer, last_frame_duration);
		bar.OnRender(g_renderer, last_frame_duration);
		score.OnRender(g_renderer, last_frame_duration);
		score_text.OnRender(g_renderer, last_frame_duration);
		timer_text.OnRender(g_renderer, last_frame_duration);
		timer.OnRender(g_renderer, last_frame_duration);

		//Render game over screen
		if (game_over)
		{
			game_over_text.OnRender(g_renderer, last_frame_duration);
			click_to_restart.OnRender(g_renderer, last_frame_duration);
		}
	}

	void UiController::Select(TileEntity& tile)
	{
		selector.Enable();
		selector.SetPosition(tile.X(), tile.Y());
	}

	void UiController::Deselect()
	{
		selector.Disable();
	}

	void UiController::UpdateScore(SDL_Renderer* g_renderer, int score_value)
	{
		std::string score_str = ToString(score_value);

		int digit_count = math::DigitCount(score_value);
		while (digit_count < 5)
		{
			score_str = "0" + score_str;
			digit_count++;
		}
		bar.Scale((float)score_value / VICTORY_SCORE);
		score.UpdateText(g_renderer, "" + score_str);
	}

	void UiController::UpdateTimer(SDL_Renderer* g_renderer, int timer_value)
	{
		int minutes = timer_value / 60;
		int seconds = timer_value - 60 * minutes;

		timer.UpdateText(g_renderer, ToString(minutes) + ":" + ToString(seconds));
	}

	void UiController::GameOver(SDL_Renderer* g_renderer, int score_value)
	{
		if (score_value >= VICTORY_SCORE)
		{
			game_over_text.UpdateText(g_renderer, "Victory!");
		}
		else
		{
			game_over_text.UpdateText(g_renderer, "Times up! ");
		}
		game_over = true;
	}

	void UiController::Restart()
	{
		game_over = false;
	}
}