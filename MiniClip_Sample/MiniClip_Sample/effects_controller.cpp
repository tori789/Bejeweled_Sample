#include "effects_controller.h"

namespace bejeweled
{
	void EffectsController::OnStart(SDL_Renderer* g_renderer)
	{
		for (const auto& explosion : effects)
		{
			explosion->OnStart(g_renderer);
		}
	}

	static bool Finished(const std::unique_ptr<ExplosionEntity>& explosion) 
	{
		return explosion->Finished();
	}

	void EffectsController::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		// purge all the finished explosions
		auto it = std::remove_if(effects.begin(), effects.end(), Finished);
		effects.erase(it, effects.end());

		// Now render
		for (const auto& explosion : effects)
		{
			explosion->OnRender(g_renderer, last_frame_duration);
		}
	}

	void EffectsController::CreateExplosion(SDL_Renderer* g_renderer, const TileEntity& tile)
	{
		assert(tile.HasPiece());
		std::unique_ptr<ExplosionEntity> explosion = std::make_unique<ExplosionEntity>(tile.X(), tile.Y(), TILE_PIXEL_SIZE, tile.Piece().PieceID());
		explosion->OnStart(g_renderer);
		effects.emplace_back(std::move(explosion));
	}

	void EffectsController::CreateScoreExplosion(SDL_Renderer* g_renderer, const TileEntity& tile, int num_pieces)
	{
		std::unique_ptr<ExplosionEntity> explosion = std::make_unique<ScoreExplosionEntity>(tile.X(), tile.Y(), TILE_PIXEL_SIZE, 0, ToString(num_pieces * SCORE_PER_PIECE));
		explosion->OnStart(g_renderer);
		effects.emplace_back(std::move(explosion));
	}
}