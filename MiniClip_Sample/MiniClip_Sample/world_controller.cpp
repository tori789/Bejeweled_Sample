#include "world_controller.h"

namespace bejeweled
{
	WorldController::WorldController(int x_tiles, int y_tiles) : destroyed_pieces(0), potential_matching_tiles{}, piece_types{}, map(), effects()
	{
	}

	WorldController::~WorldController()
	{

	}

	void WorldController::RandomFill(SDL_Renderer* g_renderer)
	{
		int moves = 0;
		while (moves <= 5)
		{
			for (int y = 0; y < MAP_TILE_SIZE_Y; y++)
			{
				for (int x = 0; x < MAP_TILE_SIZE_X; x++)
				{
					int index = x + y * MAP_TILE_SIZE_X;
					TileEntity& tile = map.TileFromIndex(index);
					PieceEntity new_piece = PieceEntity(tile.X(), tile.Y(), TILE_PIXEL_SIZE, CalculateNewPieceType(tile));

					if (tile.HasPiece())
					{
						tile.DestroyPiece();
					}
					tile.FillWithPiece(new_piece);
				}
			}
			// we now check the amount of moves we have
			moves = CalculateMoves();
		}
	}

	void WorldController::OnStart(SDL_Renderer* g_renderer)
	{
		// Initialize 5 piece types
		piece_types.emplace_back(PieceTypeContainer(0, 1));
		piece_types.emplace_back(PieceTypeContainer(0, 2));
		piece_types.emplace_back(PieceTypeContainer(0, 3));
		piece_types.emplace_back(PieceTypeContainer(0, 4));
		piece_types.emplace_back(PieceTypeContainer(0, 5));

		// Then we fill the map
		RandomFill(g_renderer);

		map.OnStart(g_renderer);
		effects.OnStart(g_renderer);
	}

	bool PotentialSwitchesToRemove(const PieceSwitch& swap)
	{
		return swap.remove;
	}

	void WorldController::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		// We will push into this vector while doing matching but we don't want to check those tiles in the current frame
		int initial_size = potential_matching_tiles.size();

		bool matched = false;

		// Iterate all potential matching tile pieces
		for (int i = 0; i < initial_size; i++)
		{
			PieceSwitch& potential_switch = potential_matching_tiles.at(i);
			TileEntity& tile0 = potential_switch.source;
			TileEntity& tile1 = potential_switch.target;

			if (tile0.HasPiece() && tile1.HasPiece() && !HasHorizontalMovingPieces(tile0) && !HasHorizontalMovingPieces(tile1) && !HasVerticalMovingPieces(tile0) && !HasVerticalMovingPieces(tile1))
			{
				matched = matched || Match(tile0, tile1, g_renderer);
				potential_switch.remove = true;
				if (matched)
				{
					break;
				}
			}
		}

		// We remove the checked pieces
		potential_matching_tiles.erase(std::remove_if(potential_matching_tiles.begin(), potential_matching_tiles.end(), PotentialSwitchesToRemove), potential_matching_tiles.end());

		if (matched)
		{
			// We have destroyed a set of pieces we need to consolidate the map and generate new pieces for the ones we destroyed
			ConsolidadeTiles();

			FillMissingTiles(g_renderer);
		}

		// We render the map tiles
		map.OnRender(g_renderer, last_frame_duration);

		// We render effects on top of the map
		effects.OnRender(g_renderer, last_frame_duration);
	}

	void WorldController::GameOver(SDL_Renderer* g_renderer)
	{
		for (int y = 0; y < MAP_TILE_SIZE_Y; y++)
		{
			for (int x = 0; x < MAP_TILE_SIZE_X; x++)
			{
				int index = x + y * MAP_TILE_SIZE_X;
				TileEntity& tile = map.TileFromIndex(index);

				DestroyAndExplodePiece(g_renderer, tile);
			}
		}
		potential_matching_tiles.clear();
		piece_types.clear();
		destroyed_pieces = 0;
	}

	void WorldController::ConsolidadeTiles()
	{
		for (int x = 0; x < MAP_TILE_SIZE_X; x++)
		{
			for (int y = 1; y < MAP_TILE_SIZE_Y; y++)
			{
				TileEntity& tile = map.TileFromXY(x, y);
				if (!tile.HasPiece())
				{
					continue;
				}
				std::reference_wrapper<TileEntity> lowest_tile_without_piece = std::ref(map.Down(tile));

				if (lowest_tile_without_piece.get().HasPiece())
				{
					continue;
				}
				while (lowest_tile_without_piece.get().Y() != 0 && !map.Down(lowest_tile_without_piece).HasPiece())
				{
					lowest_tile_without_piece = map.Down(lowest_tile_without_piece);
				}

				tile.Piece().Move(lowest_tile_without_piece.get().X(), lowest_tile_without_piece.get().Y());
				lowest_tile_without_piece.get().FillWithPiece(tile.Piece());
				tile.DestroyPiece();

				// Add the lowest tile to the list to check
				AddPotentialMatch(lowest_tile_without_piece, lowest_tile_without_piece);
			}
		}
	}

	void WorldController::FillMissingTiles(SDL_Renderer* g_renderer)
	{
		for (int x = 0; x < MAP_TILE_SIZE_X; x++)
		{
			int empty_count = 0;
			for (int y = 0; y < MAP_TILE_SIZE_Y; y++)
			{
				TileEntity& tile = map.TileFromXY(x, y);

				if (tile.HasPiece())
				{
					continue;
				}
				PieceEntity new_piece = PieceEntity(tile.X(), MAP_TILE_SIZE_Y + empty_count, TILE_PIXEL_SIZE, CalculateNewPieceType(tile));

				new_piece.OnStart(g_renderer);
				new_piece.Move(tile.X(), tile.Y());
				tile.FillWithPiece(new_piece);

				empty_count++;

				// Add the new tile to the list to check
				AddPotentialMatch(tile, tile);
			}
		}
	}

	bool WorldController::Match(TileEntity& current_tile, TileEntity& previous_tile, SDL_Renderer* g_renderer)
	{
		bool sucessfull = false;
		std::vector<std::reference_wrapper<const PieceEntity>> matched_pieces = {};
		std::vector<std::reference_wrapper<const PieceEntity>> matched;

		if (current_tile.X() != previous_tile.X())
		{
			matched = MatchHorizontal(current_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));

			matched = MatchVertical(current_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));

			matched = MatchVertical(previous_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));
		}
		else if (current_tile.Y() != previous_tile.Y())
		{
			matched = MatchVertical(current_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));

			matched = MatchHorizontal(current_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));

			matched = MatchHorizontal(previous_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));
		}
		else
		{
			matched = MatchVertical(current_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));

			matched = MatchHorizontal(current_tile);
			matched_pieces.insert(std::end(matched_pieces), std::begin(matched), std::end(matched));
		}

		if (!(matched_pieces.size() >= 3))
		{
			if (previous_tile != current_tile)
			{
				// We could not match anything we have to switch back
				AnimatedSwitch(previous_tile, current_tile);
			}
			matched_pieces.clear();
			return false;
		}

		int destroyed = 0;

		// We have a list of tiles that are matched and we need to destroy them
		for (const auto& piece : matched_pieces)
		{
			TileEntity& tile = map.TileFromXY(piece.get().X(), piece.get().Y());

			// matched_pieces vector can have the same pieces
			if (tile.HasPiece())
			{
				DestroyAndExplodePiece(g_renderer, tile);
				destroyed++;
			}
		}

		// We need to explode score exactly on the piece that made the match.
		if (current_tile.HasPiece())
		{
			ExplodeScore(g_renderer, previous_tile, destroyed);
		}
		else
		{
			ExplodeScore(g_renderer, current_tile, destroyed);
		}

		matched_pieces.clear();
		return true;
	}

	// Creates a number explosion
	void WorldController::ExplodeScore(SDL_Renderer* g_renderer, const TileEntity& tile, int number_of_pieces)
	{
		assert(number_of_pieces > 0);
		effects.CreateScoreExplosion(g_renderer, tile, number_of_pieces);
	}

	// Destroys a piece and instantiates a explosion
	void WorldController::DestroyAndExplodePiece(SDL_Renderer* g_renderer, TileEntity& tile)
	{
		effects.CreateExplosion(g_renderer, tile);
		tile.DestroyPiece();
		destroyed_pieces++;
	}

	// We switch the tile pieces
	void WorldController::Switch(TileEntity& current_tile, TileEntity& next_tile)
	{
		PieceEntity aux_current = current_tile.Piece();
		PieceEntity aux_next = next_tile.Piece();
		current_tile.FillWithPiece(aux_next);
		next_tile.FillWithPiece(aux_current);
	}

	// We switch the tile pieces and set the pieces to move 
	void WorldController::AnimatedSwitch(TileEntity& current_tile, TileEntity& next_tile)
	{
		current_tile.Piece().Move(next_tile.X(), next_tile.Y());
		next_tile.Piece().Move(current_tile.X(), current_tile.Y());
		Switch(current_tile, next_tile);
	}

	std::vector<std::reference_wrapper<const PieceEntity>> WorldController::MatchVertical(TileEntity& current_tile)
	{
		// Check vertical column
		int x_current_index = current_tile.X();

		// Number of matched equal tiles
		int number_equal_tiles = 1;

		int current_piece = 0;
		std::vector<std::reference_wrapper<const PieceEntity>> matched_pieces{};

		for (int y = 0; y < MAP_TILE_SIZE_Y; y++)
		{
			int index = x_current_index + y * MAP_TILE_SIZE_X;

			assert(index >= 0 && index < TOTAL_TILE_COUNT);

			TileEntity& tile_to_check = map.TileFromIndex(index);

			if (tile_to_check.Piece().SameID(current_piece))
			{
				matched_pieces.emplace_back(std::cref(tile_to_check.Piece()));
				number_equal_tiles++;
			}
			else
			{
				if (matched_pieces.size() < 3)
				{
					matched_pieces.clear();
				}
				else
				{
					break;
				}

				current_piece = tile_to_check.Piece().PieceID();
				matched_pieces.emplace_back(std::cref(tile_to_check.Piece()));
				number_equal_tiles = 1;
			}
		}

		if (matched_pieces.size() < 3)
		{
			matched_pieces.clear();
		}

		return matched_pieces;
	}

	std::vector<std::reference_wrapper<const PieceEntity>> WorldController::MatchHorizontal(TileEntity& current_tile)
	{
		// Check horizontal line
		int y_current_index = current_tile.Y();

		// Number of matched equal tiles
		int number_equal_tiles = 1;

		int current_piece = 0;
		std::vector<std::reference_wrapper<const PieceEntity>> matched_pieces{};

		for (int x = 0; x < MAP_TILE_SIZE_X; x++)
		{
			int index = x + y_current_index * MAP_TILE_SIZE_X;

			assert(index >= 0 && index < TOTAL_TILE_COUNT);

			TileEntity& tile_to_check = map.TileFromIndex(index);

			if (tile_to_check.Piece().SameID(current_piece))
			{
				matched_pieces.emplace_back(std::cref(tile_to_check.Piece()));
				number_equal_tiles++;
				continue;
			}

			if (matched_pieces.size() >= 3)
			{
				break;
			}

			matched_pieces.clear();

			current_piece = tile_to_check.Piece().PieceID();
			matched_pieces.emplace_back(std::cref(tile_to_check.Piece()));
			number_equal_tiles = 1;
		}

		if (matched_pieces.size() < 3)
		{
			matched_pieces.clear();
		}

		return matched_pieces;
	}

	void WorldController::AddPotentialMatch(TileEntity& current_tile, TileEntity& next_tile)
	{
		potential_matching_tiles.emplace_back(PieceSwitch{ current_tile, next_tile });

	}

	TileEntity& WorldController::TileFromMouseCoordinates(int x, int y)
	{
		int x_index = (int)std::round(x / TILE_PIXEL_SIZE);
		x_index = math::Clamp(x_index, 0, MAP_TILE_SIZE_X - 1);

		int y_index = (int)std::round(y / TILE_PIXEL_SIZE);
		y_index = math::Clamp(y_index, 0, MAP_TILE_SIZE_Y - 1);

		int index = x_index + y_index * MAP_TILE_SIZE_X;

		assert(index >= 0 && index <= TOTAL_TILE_COUNT - 1);

		return map.TileFromIndex(index);
	}

	bool WorldController::HasHorizontalMovingPieces(TileEntity& tile)
	{
		bool has_moving_pieces = false;
		int y = tile.Y();
		for (int x = 0; x < MAP_TILE_SIZE_X; x++)
		{
			int index = x + y * MAP_TILE_SIZE_X;
			TileEntity& tile = map.TileFromIndex(index);
			if (tile.HasPiece() && tile.Piece().IsMoving())
			{
				has_moving_pieces = true;
				break;
			}
		}
		return has_moving_pieces;
	}

	bool WorldController::HasVerticalMovingPieces(TileEntity& tile)
	{
		bool has_moving_pieces = false;
		int x = tile.X();
		for (int y = 0; y < MAP_TILE_SIZE_Y; y++)
		{
			int index = x + y * MAP_TILE_SIZE_X;
			TileEntity& tile = map.TileFromIndex(index);
			if (tile.HasPiece() && tile.Piece().IsMoving())
			{
				has_moving_pieces = true;
				break;
			}
		}
		return has_moving_pieces;
	}

	int WorldController::CalculateMoves()
	{
		int possible_moves = 0;
		for (unsigned int i = 0; i < TOTAL_TILE_COUNT; i++)
		{
			TileEntity& tile = map.TileFromIndex(i);

			// Check moving up or down
			if (tile.Y() < MAP_TILE_SIZE_Y - 1)
			{
				TileEntity& next_tile = map.Up(tile);
				Switch(tile, next_tile);
				if (MatchVertical(tile).size() >= 3) { possible_moves++; }
				if (MatchHorizontal(tile).size() >= 3) { possible_moves++; }
				if (MatchHorizontal(next_tile).size() >= 3) { possible_moves++; }
				Switch(tile, next_tile);
			}

			if (tile.Y() > 0)
			{
				TileEntity& next_tile = map.Down(tile);
				Switch(tile, next_tile);
				if (MatchVertical(tile).size() >= 3) { possible_moves++; }
				if (MatchHorizontal(tile).size() >= 3) { possible_moves++; }
				if (MatchHorizontal(next_tile).size() >= 3) { possible_moves++; }
				Switch(tile, next_tile);
			}

			// Check moving right or left
			if (tile.X() < MAP_TILE_SIZE_X - 1)
			{
				TileEntity& next_tile = map.Right(tile);
				Switch(tile, next_tile);
				if (MatchVertical(tile).size() >= 3) { possible_moves++; }
				if (MatchVertical(next_tile).size() >= 3) { possible_moves++; }
				if (MatchHorizontal(tile).size() >= 3) { possible_moves++; }
				Switch(tile, next_tile);
			}

			if (tile.X() > 0)
			{
				TileEntity& next_tile = map.Left(tile);
				Switch(tile, next_tile);
				if (MatchVertical(tile).size() >= 3) { possible_moves++; }
				if (MatchVertical(next_tile).size() >= 3) { possible_moves++; }
				if (MatchHorizontal(tile).size() >= 3) { possible_moves++; }
				Switch(tile, next_tile);
			}
		}
		return possible_moves;
	}

	bool WorldController::CheckNeighbors(int piece_type, TileEntity& tile, int amount)
	{
		bool match = false;
		match = match || CheckNeighbors(piece_type, tile, 1, 0, amount);
		match = match || CheckNeighbors(piece_type, tile, -1, 0, amount);
		match = match || CheckNeighbors(piece_type, tile, 0, 1, amount);
		match = match || CheckNeighbors(piece_type, tile, 0, -1, amount);
		return match;
	}

	// This function iterates from the current tile position up to a certain distance, checking if we have the same piece type
	bool WorldController::CheckNeighbors(int piece_type, TileEntity& tile, int x_increment, int y_increment, int distance)
	{
		int current_piece_type = piece_type;

		int x = math::Clamp(tile.X() + x_increment, 0, MAP_TILE_SIZE_X - 1);
		int y = math::Clamp(tile.Y() + y_increment, 0, MAP_TILE_SIZE_Y - 1);

		// If after the increment we are still acessing the same indexes as the starting tile
		if (x == tile.X() && y == tile.Y())
		{
			return false;
		}

		int curr_distance = 1;
		while (curr_distance < distance)
		{
			if (x >= 0 && y >= 0 && x < MAP_TILE_SIZE_X && y < MAP_TILE_SIZE_X)
			{
				TileEntity& tile = map.TileFromXY(x, y);

				if (!tile.HasPiece() || !tile.Piece().SameID(current_piece_type))
				{
					return false;
				}

				x += x_increment;
				y += y_increment;
			}
			else
			{
				return false;
			}
			curr_distance++;
		}
		return true;
	}

	int WorldController::CalculateNewPieceType(TileEntity& tile)
	{
		PieceTypeContainer* chosen_piece_count = &piece_types.at(0);
		
		int rollMaxCount = 12;
		int currentRoll = 0;

		//TODO we can improve this function by using an auxiliar vector with a copy of the piece_to_check vector
		while (currentRoll <= rollMaxCount)
		{
			int rand_num = math::RandomBetween(0, (int)piece_types.size() - 1);

			// We randomly choose one of the 5 piece types to check their probability
			PieceTypeContainer& piece_to_check = piece_types.at(rand_num);

			if (CheckNeighbors(piece_to_check.piece_id, tile, 2))
			{
				continue;
			} 
			if (math::BernoulliRandom(piece_to_check.probability))
			{
				chosen_piece_count = &piece_to_check;
				break;
			}
			currentRoll++;
		}

		// Update the density
		chosen_piece_count->count++;
		chosen_piece_count->density = chosen_piece_count->count / (float)TOTAL_TILE_COUNT;

		// We lower the chosen piece probability by 10% and increase all others by 10%
		chosen_piece_count->probability = (int)math::Clamp(chosen_piece_count->probability - 10, 0, 100);

		for (auto& piece_count : piece_types)
		{
			if (piece_count.piece_id == chosen_piece_count->piece_id)
			{
				piece_count.probability = (int)math::Clamp(piece_count.probability + 10, 0, 100);
			}
		}

		return chosen_piece_count->piece_id;
	}

	bool WorldController::HasPotentialMatches() const
	{
		return potential_matching_tiles.size() > 0;
	}

	int WorldController::DestroyedPieces() const
	{
		return destroyed_pieces;
	}
}