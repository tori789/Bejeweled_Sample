#pragma once
#include "texture.h"
#include "SDL_ttf.h"

namespace bejeweled 
{
	/*
		Holds a SDL_Texture, responsible for loading a font and using it to render a text
	*/
	class TextTexture : public Texture
	{
		std::string text;

	public:
		TextTexture(std::string text, size_t id);

		void LoadFromFile(SDL_Renderer* g_renderer, std::string file_name) override;
	};
}