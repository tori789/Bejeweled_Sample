#include "tile_entity.h"

namespace bejeweled
{
	void TileEntity::OnStart(SDL_Renderer* g_renderer)
	{
		quad.texture = TextureManager::GetInstance().CreateTexture(g_renderer, "sprite_sheet", SPRITE_SHEET_TEXTURE_ID);
		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
		quad.originrect = { 0, 0, quad.x_size, quad.y_size };
	}

	void TileEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		//Render background tile texture
		Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, quad.originrect, TILE_PIXEL_SIZE);
	}

	void TileEntity::FillWithPiece(PieceEntity& new_piece)
	{
		piece = new_piece;
	}

	void TileEntity::DestroyPiece()
	{
		piece = PieceEntity();
	}

	bool TileEntity::HasPiece() const
	{
		return piece.Valid();
	}

	bool TileEntity::IsAdjacent(TileEntity& other_tile) const
	{
		// Not equal
		bool equal_x = quad.X() == other_tile.quad.X();
		bool equal_y = quad.Y() == other_tile.quad.Y();
		// Is adjacent to the left or to the right
		bool adjacent_left_right = quad.X() == other_tile.quad.X() + 1 || quad.X() == other_tile.quad.X() - 1;
		// Is adjacent upwards or downwards
		bool adjacent_up_down = quad.Y() == other_tile.quad.Y() + 1 || quad.Y() == other_tile.quad.Y() - 1;

		return (!equal_x || !equal_y) && ((adjacent_left_right && !adjacent_up_down && equal_y) || (adjacent_up_down && !adjacent_left_right && equal_x));
	}

	const PieceEntity& TileEntity::Piece() const
	{
		return piece;
	}

	PieceEntity& TileEntity::Piece()
	{
		return piece;
	}

	int TileEntity::X() const
	{
		return quad.X();
	}

	int TileEntity::Y() const
	{
		return quad.Y();
	}
}