#include "score_explosion_entity.h"

namespace bejeweled
{
	ScoreExplosionEntity::~ScoreExplosionEntity() 
	{
		TextureManager::GetInstance().DestroyTexture(quad.texture->ID());
	}

	void ScoreExplosionEntity::OnStart(SDL_Renderer* g_renderer)
	{
		std::string font = "pixel_font";
		quad.texture = TextureManager::GetInstance().CreateTextTexture(g_renderer, font, text);

		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
	}

	void ScoreExplosionEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		float percentageComplete = (float)time_moving / animation_time;

		// We lerp our scale towards 0
		float new_scale = math::Lerp(1.0f, 0.0f, math::CubicEaseInOut(percentageComplete));

		if (percentageComplete >= 1.0f)
		{
			finished = true;
			time_moving = 0;
		}
		else
		{
			int new_x_size = (int)(quad.x_size * new_scale);
			int new_y_size = (int)(quad.y_size * new_scale);

			quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE + HALF_TILE_PIXEL_SIZE - (int)(new_x_size * 0.5f),
				quad.y_pos_index * TILE_PIXEL_SIZE + HALF_TILE_PIXEL_SIZE + (int)(new_y_size * 0.5f),
				new_x_size, new_y_size };

			SDL_SetTextureAlphaMod(quad.texture->SDLTexture(), (int)(new_scale * 255));
			time_moving += last_frame_duration;
			Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, 0);
		}
	}

	bool ScoreExplosionEntity::Finished() const
	{
		return finished;
	}
}