#include "game_manager.h"

using namespace bejeweled;

int main(int argc, char* args[])
{
	GameManager manager{};

	// Responsible for creating a SDL window
	manager.OnInit();

	// Responsible for creating the game session
	manager.OnStart();

	// Responsible for the games update loop
	manager.OnRender();

	// Responsible for cleaning everything before we exit the application
	manager.OnDelete();

    return 0;
}