#pragma once

#include <iostream> 
#include <SDL_image.h>
#include <memory>  

namespace bejeweled 
{
	/**
		Holds a SDL_Texture, responsible for loading a texture from a filename
	*/
	class Texture
	{
	protected:
		SDL_Texture *sdl_texture;
		std::string file;

		int width;
		int height;

		size_t id;

	public:
		Texture(const Texture&) = default;
		Texture& operator= (const Texture&) = default;

		Texture(size_t id) : width(0), height(0), sdl_texture(nullptr), id(id){}
		virtual ~Texture();
		virtual void LoadFromFile(SDL_Renderer* g_renderer, std::string file_name);

		bool Valid() const;

		std::string TextureFilename() const;
		SDL_Texture* SDLTexture() const;

		int Width() const;
		int Height() const;

		size_t ID() const;
	};
}