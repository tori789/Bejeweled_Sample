#include "bar_entity.h"

namespace bejeweled
{
	void BarEntity::OnStart(SDL_Renderer* g_renderer)
	{
		quad.texture = TextureManager::GetInstance().CreateTexture(g_renderer, "bar");
		back_texture = TextureManager::GetInstance().CreateTexture(g_renderer, "bar_background");
		backrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
		quad.fillrect = backrect;
		quad.originrect = { 0, 0, quad.texture->Width(), quad.texture->Height() };
	}

	void BarEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		// Red texture
		Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, quad.originrect, TILE_PIXEL_SIZE);
		// Outline
		Render(g_renderer, back_texture->SDLTexture(), backrect, TILE_PIXEL_SIZE);
	}

	void BarEntity::Scale(float scale)
	{
		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, (int)(quad.y_size * scale) };
		quad.originrect = { 0, 0, quad.texture->Width(), (int)(quad.texture->Height() * scale) };
	}
}