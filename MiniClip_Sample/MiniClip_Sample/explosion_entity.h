#pragma once

#include "math.h"
#include "quad_entity.h"
#include "texture_manager.h"
#include "extensions.h"

namespace bejeweled
{
	/*
		Renders a explosion of a piece - scales it to zero.
	*/
	class ExplosionEntity : public Renderable
	{
	protected:
		int piece_id;

		// Time in miliseconds it takes the explosion to scale to zero.
		int animation_time;

		// Time in miliseconds the piece has started moving.
		int time_moving;

		// When scale hits zero finished equals true.
		bool finished;

		QuadEntity quad;

	public:
		ExplosionEntity(const ExplosionEntity&) = default;
		ExplosionEntity& operator= (const ExplosionEntity&) = default;

		ExplosionEntity(int x, int y, int size, int piece_id, int animation_time = 200, bool enabled = true) :
			quad(x, y, size, size),
			piece_id(piece_id),
			animation_time(animation_time), time_moving(0), finished(false) {}

		~ExplosionEntity() {}

		void OnStart(SDL_Renderer* g_renderer) override;
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) override;

		bool Finished() const;
	};
}