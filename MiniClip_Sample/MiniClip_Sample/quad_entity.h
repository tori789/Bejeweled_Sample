#pragma once

#include <SDL.h>
#include <memory>
#include "constants.h"
#include "renderable.h"
#include "texture.h"

namespace bejeweled 
{
	/*
		Generic class representing a 2D rectangle.
	*/
	struct QuadEntity
	{
		// Starting position indexes
		int x_pos_index;
		int y_pos_index;

		// Size in pixels
		int x_size;
		int y_size;

		const Texture* texture;

		// Final size in pixels.
		SDL_Rect fillrect;
		// Original texture size and location where to get the color information.
		SDL_Rect originrect;

		QuadEntity() : x_pos_index(0), y_pos_index(0), x_size(0), y_size(0), texture(nullptr), fillrect(), originrect() {}
		QuadEntity(int x, int y, int x_size, int y_size) : x_pos_index(x), y_pos_index(y), x_size(x_size), y_size(y_size), texture(nullptr), fillrect(), originrect() {}

		int X() const
		{
			return x_pos_index;
		}

		int Y() const
		{
			return y_pos_index;
		}

		bool Valid() const 
		{
			return x_size >= 0 && y_size >= 0;
		}
	};
}