#include "piece_entity.h"

namespace bejeweled
{
	void PieceEntity::OnStart(SDL_Renderer* g_renderer)
	{
		quad.texture = TextureManager::GetInstance().CreateTexture(g_renderer, "sprite_sheet", SPRITE_SHEET_TEXTURE_ID);
		quad.fillrect = { x_current_pixel, y_current_pixel, quad.x_size, quad.y_size };
		quad.originrect = { TILE_PIXEL_SIZE * piece_id, 0, quad.x_size, quad.y_size };
	}

	void PieceEntity::OnRender(SDL_Renderer* g_renderer, int last_frame_duration)
	{
		if (updatePosition)
		{
			float percentageComplete = (float)time_moving / (animation_time * math::Magnitude(x_goal_index - x_previous_goal_index, y_goal_index - y_previous_goal_index));

			// We lerp our position towards a given goal
			x_current_pixel = (int)(math::Lerp((float)quad.x_pos_index * TILE_PIXEL_SIZE, (float)x_goal_index * TILE_PIXEL_SIZE, math::QuadraticEaseIn(percentageComplete)));
			y_current_pixel = (int)(math::Lerp((float)quad.y_pos_index * TILE_PIXEL_SIZE, (float)y_goal_index * TILE_PIXEL_SIZE, math::QuadraticEaseIn(percentageComplete)));

			if (percentageComplete >= 1.0f)
			{
				// We have arrived
				quad.x_pos_index = x_goal_index;
				quad.y_pos_index = y_goal_index;

				updatePosition = false;
				time_moving = 0;
				quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
			}
			else
			{
				quad.fillrect = { x_current_pixel, y_current_pixel, quad.x_size, quad.y_size };
				time_moving += last_frame_duration;
			}
		}

		//Render piece texture
		if (enabled)
		{
			Render(g_renderer, quad.texture->SDLTexture(), quad.fillrect, quad.originrect, TILE_PIXEL_SIZE);
		}
	}

	void PieceEntity::SetPosition(int x_new_pos, int y_new_pos)
	{
		x_current_pixel = x_goal_index * TILE_PIXEL_SIZE;
		y_current_pixel = quad.y_pos_index * TILE_PIXEL_SIZE;

		x_previous_goal_index = x_goal_index;
		y_previous_goal_index = y_goal_index;

		x_goal_index = x_new_pos;
		y_goal_index = y_new_pos;

		quad.x_pos_index = x_goal_index;
		quad.y_pos_index = y_goal_index;

		quad.fillrect = { quad.x_pos_index * TILE_PIXEL_SIZE, quad.y_pos_index * TILE_PIXEL_SIZE, quad.x_size, quad.y_size };
	}

	void PieceEntity::Move(int x_new_pos, int y_new_pos)
	{
		if (!updatePosition)
		{
			x_previous_goal_index = x_goal_index;
			y_previous_goal_index = y_goal_index;
		}

		x_goal_index = x_new_pos;
		y_goal_index = y_new_pos;

		updatePosition = true;
	}

	// The texture filename acts as the piece type
	int PieceEntity::PieceID() const
	{
		return piece_id;
	}

	bool PieceEntity::SameID(int other_piece_type) const
	{
		return piece_id == other_piece_type;
	}

	bool PieceEntity::IsMoving() const
	{
		return updatePosition;
	}

	void PieceEntity::Enable()
	{
		enabled = true;
	}

	void PieceEntity::Disable()
	{
		enabled = false;
	}

	int PieceEntity::X() const
	{
		return x_goal_index;
	}

	int PieceEntity::Y() const
	{
		return y_goal_index;
	}

	int PieceEntity::PreviousX() const
	{
		return x_previous_goal_index;
	}

	int PieceEntity::PreviousY() const
	{
		return y_previous_goal_index;
	}

	bool PieceEntity::Valid() const
	{
		return piece_id != -1;
	}
}