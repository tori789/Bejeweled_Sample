#pragma once

namespace bejeweled
{
	class TileEntity;

	/*
		Stores two references for a potential piece switch
	*/
	struct PieceSwitch
	{
		std::reference_wrapper<TileEntity> source;
		std::reference_wrapper<TileEntity> target;
		bool remove = false;

		bool operator ==(const PieceSwitch& other)
		{
			return source.get() == target.get();
		}

		void operator =(const PieceSwitch& other)
		{
			source = other.source;
			target = other.target;
		}
	};
}