#pragma once

#include "math.h"
#include "quad_entity.h"
#include "texture_manager.h"
#include "extensions.h"
#include "explosion_entity.h"

namespace bejeweled
{
	/*
	Renders score scaling to zero.
	*/
	class ScoreExplosionEntity : public ExplosionEntity
	{
		std::string text;

	public:
		ScoreExplosionEntity(int x, int y, int size, int piece_id, std::string text, int animation_time = 400, bool enabled = true) :
			ExplosionEntity(x, y, size, piece_id, animation_time, enabled),
			text(text) {}

		~ScoreExplosionEntity();

		void OnStart(SDL_Renderer* g_renderer);
		void OnRender(SDL_Renderer* g_renderer, int last_frame_duration);

		bool Finished() const;
	};
}