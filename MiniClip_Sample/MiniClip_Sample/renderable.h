#pragma once

namespace bejeweled 
{
	/*
		Entities like maps, tiles, pieces among other elements have a comon start and render method.
	*/
	class Renderable
	{

	public:
		virtual ~Renderable() {}
		virtual void OnStart(SDL_Renderer* g_renderer) = 0;
		virtual void OnRender(SDL_Renderer* g_renderer, int last_frame_duration) = 0;
	};
}