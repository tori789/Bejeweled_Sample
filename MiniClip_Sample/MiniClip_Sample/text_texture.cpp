#include "text_texture.h"

namespace bejeweled 
{
	TextTexture::TextTexture(std::string text, size_t id) : Texture(id)
	{
		this->id = id;
		this->text = text;
	}

	void TextTexture::LoadFromFile(SDL_Renderer* g_renderer, std::string file_name)
	{
		this->file = file_name;

		//this opens a font style and sets a size
		TTF_Font* Sans = TTF_OpenFont(file_name.c_str(), 24);

		if (Sans == nullptr)
		{
			std::cout << "Unable to create text texture from " + file_name + " SDL Error: " + TTF_GetError() << std::endl;
			return;
		}

		SDL_Color White = { 255, 255, 255 };

		// Create a SDL surface with the loaded font and the current text
		auto sdl_surface_image = TTF_RenderText_Blended(Sans, text.c_str(), White);

		auto sucessfull = false;
		if (sdl_surface_image == nullptr)
		{
			std::cout << "Unable to create text texture from " + file_name + " SDL Error: " + SDL_GetError() << std::endl;
			return;
		}

		//Create texture from surface pixels
		sdl_texture = SDL_CreateTextureFromSurface(g_renderer, sdl_surface_image);

		if (sdl_texture == nullptr)
		{
			std::cout << "Unable to create texture from " + file_name + " SDL Error: " + SDL_GetError() << std::endl;
			return;
		}

		SDL_QueryTexture(sdl_texture, NULL, NULL, &width, &height);

		TTF_CloseFont(Sans);

		SDL_FreeSurface(sdl_surface_image);
	}
}