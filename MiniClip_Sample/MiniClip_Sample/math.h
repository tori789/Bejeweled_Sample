#pragma once
#include <algorithm>
#include <math.h>
#include <random>
#include <assert.h> 

namespace math 
{
	inline float Magnitude(int x, int y)
	{
		return std::sqrtf((float)std::pow(x, 2) + (float)std::pow(y, 2));
	}

	template<typename T> inline T Clamp(T n, T lower, T upper)
	{
		return std::max(lower, std::min(n, upper));
	}

	template<typename T> inline T Lerp(T a, T b, T f)
	{
		return a + f * (b - a);
	}

	// We are going to roll with a probability
	inline bool BernoulliRandom(int probability)
	{
		std::random_device rd;
		std::uniform_int_distribution<int> distribution(1, 100);
		std::mt19937 engine(rd()); // Mersenne twister MT19937

		int value = distribution(engine);
		if (value <= probability)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	inline int RandomBetween(int min, int max)
	{
		return rand() % (max - min + 1) + min;
	}

	// y = x^2
	template<typename T> T QuadraticEaseIn(T p)
	{
		return p * p;
	}

	// y = -x^2 + 2x
	template<typename T> T QuadraticEaseOut(T p)
	{
		return -(p * (p - 2));
	}

	// y = (1/2)((2x)^3)       
	// y = (1/2)((2x-2)^3 + 2)
	template<typename T> T CubicEaseInOut(T p)
	{
		if (p < 0.5f)
		{
			return 4 * p * p * p;
		}
		else
		{
			T f = ((2 * p) - 2);
			return 0.5f * f * f * f + 1;
		}
	}

	inline int DigitCount(int number)
	{
		int digit_count = 0;

		do
		{
			digit_count++;
			number /= 10;
		} while (number > 0);

		return digit_count;
	}
}